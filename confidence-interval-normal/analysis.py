#!/usr/bin/env python3

import plotly.offline as py
import plotly.graph_objs as go
from scipy.stats import chi2, norm
from numpy import random


##
# sigma - variance of normal distribution
# gamma - confidence interval level
# N - max number of elements in sample
# 
# Builds trace for average lengths of confidence interval for normal distribution variance in case statistics $S^2$ is used.
##
def build_trace_chi2_interval(sigma, gamma, N):
    size, step = 10, 10
    x = []
    y = []
    deviations = []
    
    while size < N:
        ql, qr = (1 + gamma) / 2, (1 - gamma) / 2
        l, r = 1 / chi2.ppf(q=ql, df=size), 1 / chi2.ppf(q=qr, df=size)
        x.append(size)
        y.append((r - l) * size * sigma ** 2)
        size += step
        if size == step * 10:
            step *= 10

    return go.Scatter(x=x, y=y, mode='lines', name='sigma={}, gamma={}, chi2'.format(sigma, gamma))


##
# sigma - variance of normal distribution
# gamma - confidence interval level
# N - max number of elements in sample
# 
# Builds trace for average lengths of confidence interval for normal distribution variance in case statistics $overline(X)^2$ is used.
##
def build_trace_norm_interval(sigma, gamma, N):
    size, step = 10, 10
    x = []
    y = []
    deviations = []
    
    while size < N:
        ql, qr = (3 + gamma) / 4, (3 - gamma) / 4
        l, r = 1 / norm.ppf(q=ql), 1 / norm.ppf(q=qr)
        x.append(size)
        y.append((r - l) * sigma ** 2)
        size += step
        if size == step * 10:
            step *= 10

    return go.Scatter(x=x, y=y, mode='lines', name='sigma={}, gamma={}, norm'.format(sigma, gamma))


##
# sigma - variance of normal distribution
# gamma - confidence interval level
# N - max number of elements in sample
# 
# Builds plot to compare two types of confidence intervals depending on which statistics is used.
##
def build_plot(sigma, gamma, N):
    traces = [
        build_trace_chi2_interval(sigma, gamma, N), 
        build_trace_norm_interval(sigma, gamma, N)
    ]
    layout = go.Layout(
        title='Compirison of confidence interval lengths depending on sample size, log plot',
        xaxis=dict(
            title='sample size',
            type='log'
        ),
        yaxis=dict(
            title='length',
        )
    )
    plot = go.Figure(data=traces, layout=layout)
    py.plot(plot, filename='comparison_sigma={},gamma={}.html'.format(sigma, gamma))


if __name__ == '__main__':
    build_plot(10, 0.975, 100000)
    build_plot(10, 0.95, 100000)
    build_plot(10, 0.925, 100000)
    build_plot(10, 0.9, 100000)
    build_plot(10, 0.85, 100000)
    build_plot(10, 0.8, 100000)

