#!/usr/bin/env python3

import plotly.offline as py
import plotly.graph_objs as go
from numpy import random
from math import factorial


##
# n - number of elements in sample
# theta - parameter of uniform distribution, i.e. U[0, theta]
# k - moment number
# iterations - number of samples needed to evaluate standard deviation
#
# Returns standard deviation for estimation of distribution parameter.
##
def analyse_uniform_distribution(n, theta, k, iterations):
    deviation = 0
    for iteration in range(iterations):
        estimation = 0
        for value in random.uniform(low=0, high=theta, size=n):
            estimation += (value ** k) / n
        estimation = ((k + 1) * estimation) ** (1 / k)
        deviation += (estimation - theta) ** 2
    return (deviation / iterations) ** 0.5


##
# n - number of elements in sample
# theta - parameter of uniform distribution, i.e. U[0, theta]
# k - moment number
# iterations - number of samples needed to evaluate standard deviation
#
# Returns standard deviation for estimation of distribution parameter.
##
def analyse_exponential_distribution(n, theta, k, iterations):
    deviation = 0
    for iteration in range(iterations):
        estimation = 0
        for value in random.exponential(scale=theta, size=n):
            estimation += (value ** k) / n
        estimation = (estimation / factorial(k)) ** (1 / k)
        deviation += (estimation - theta) ** 2
    return (deviation / iterations) ** 0.5


##
# n - number of elements in sample
# theta - parameter of uniform distribution, i.e. U[0, theta]
# K - array of moment numbers we want to analyse
# iterations - number of samples needed to evaluate standard deviation
# 
# Builds trace for deviations of uniform distribution parameter estimations depending on moment number
##
def build_trace_uniform_distribution(n, theta, K, iterations):
    deviations = []
    for k in K:
        deviations.append(analyse_uniform_distribution(n, theta, k, iterations))
    return go.Scatter(x=K, y=deviations, mode='lines', name='n={}, theta={}'.format(n, theta))


##
# theta - parameter of uniform distribution, i.e. U[0, theta]
# iterations - number of samples needed to evaluate standard deviation
# 
# Builds plot for deviations of uniform distribution parameter estimations depending on moment number and number of elements
##
def build_uniform_plot(theta, iterations):
    traces = []
    for n in range(100, 2000, 200):
        K = list(range(1, 11))
        traces.append(build_trace_uniform_distribution(n, theta, K, iterations))
    layout = go.Layout(
        title='Uniform Distribution',
        xaxis=dict(
            title='k'
        ),
        yaxis=dict(
            title='Standard deviation'
        )
    )
    plot = go.Figure(data=traces, layout=layout)
    py.plot(plot, filename='uniform_distribution.html')


##
# n - number of elements in sample
# theta - parameter of uniform distribution, i.e. U[0, theta]
# K - array of moment numbers we want to analyse
# iterations - number of samples needed to evaluate standard deviation
# 
# Builds trace for deviations of exponential distribution parameter estimations depending on moment number
##
def build_trace_exponential_distribution(n, theta, K, iterations):
    deviations = []
    for k in K:
        deviations.append(analyse_exponential_distribution(n, theta, k, iterations))
    return go.Scatter(x=K, y=deviations, mode='lines', name='n={}, theta={}'.format(n, theta))


##
# theta - parameter of uniform distribution, i.e. U[0, theta]
# iterations - number of samples needed to evaluate standard deviation
# 
# Builds plot for deviations of exponential distribution parameter estimations depending on moment number and number of elements
##
def build_exponential_plot(theta, iterations):
    traces = []
    for n in range(100, 2000, 200):
        K = list(range(1, 11))
        traces.append(build_trace_exponential_distribution(n, theta, K, iterations))
    layout = go.Layout(
        title='Exponential Distribution',
        xaxis=dict(
            title='k'
        ),
        yaxis=dict(
            title='Standard deviation'
        )
    )
    plot = go.Figure(data=traces, layout=layout)
    py.plot(plot, filename='exponential_distribution.html')


# Here you can pass your own parameters for distributions, as well as number of iterations needed to calculate standard deviation
if __name__ == '__main__':
    build_uniform_plot(100, 100)
    build_exponential_plot(1, 100)

