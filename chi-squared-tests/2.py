#!/usr/bin/env python3

import numpy as np
from scipy.stats import chi2, binom


def argmin(f, l, r, steps):
    d = (r - l) / (steps + 1)
    
    x = l + d
    xmin, fxmin = 0, 0
    for i in range(steps):
        fx = f(x)
        if i == 0 or fx < fxmin:
            fxmin = fx
            xmin = x
        x += d

    return xmin, fxmin


def f(nu, p):
    s = len(nu)
    n = sum(nu)

    fx = 0
    for i in range(s):
        est_i = binom.pmf(i, s - 1, p) * n
        fx += (nu[i] - est_i)**2 / est_i
    return fx


##
# Pearson's chi-squared test of goodness of fit
##
def main():
    # nu_k = number of families with exactly k boys
    nu = np.array([476, 1017, 527])
    s = len(nu)

    estimated_params = 1
    _, ppf = argmin(lambda p : f(nu, p), 0, 1, 1000)

    alpha = 0.05
    accept = chi2.cdf(ppf, df=s-estimated_params-1) < 1 - alpha
    print(f'Hypothesis accepted at the confidence level {alpha}: {accept}')
    

if __name__ == '__main__':
    main()
