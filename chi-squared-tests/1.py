#!/usr/bin/env python3

import numpy as np
from scipy.stats import chi2

##
# Pearson's chi-squared test of homogeneity
##
def main():
    n = 390000
    N = 2 * n

    nu = np.array([[3.85, 31.13, 32.96, 29.92, 2.13],
                   [7.92, 36.16, 25.09, 27.49, 3.34]])

    nu[0] *= n / 100.0
    nu[1] *= n / 100.0
    p = (nu[0] + nu[1]) / N

    ppf = 0
    for i in range(2):
        for j in range(5):
            ppf += (nu[i][j] - p[j] * n)**2 / (p[j] * n)

    alpha = 0.05
    for i in range(10):
        accept = chi2.cdf(ppf, df=4) < 1 - alpha
        print(f'Hypothesis accepted at the confidence level {alpha}: {accept}')
        alpha /= 2


if __name__ == '__main__':
    main()

